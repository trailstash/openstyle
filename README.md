# OpenTrailStash [![BSD licensed](https://img.shields.io/badge/license-BSD-blue.svg)](https://github.com/maputnik/osm-liberty/blob/gh-pages/LICENSE.md)

<img align="right" height="128" alt="Icon of map with a magnifying glass" src="static/logo.svg" />
A free trail map made for unpaved bicycle exploration with world coverage using readily available
datasets that can also be self-hosted.

It consists of a [styleJS](./stylejs.md) style for [MapLibre
GL](https://maplibre.org/projects/maplibre-gl-js/) and a
[Planetiler](https://github.com/onthegomap/planetiler) Profile to
generate vector tiles.
<br>
<br>
**[View OpenTrailStash](https://open.trailsta.sh)**
<br>
<br>

## The OpenTrailStash OSM highway classification system

The main distinguising feature of OpenTrailStash is how `highway` features from OSM are
categorized.

The main principle behind the classification systme is that the OSM `highway` tags for
non-motorized use don't align with how a map should classify such features. Namely, OpenTrailStash
classifies them into two main categories: paths & trails.

## Tile Layers

### Highway

This is the most important layer of OpenTrailStash. It contains all roads, paths, trails, tracks,
etc. It is a custom schema implementing the OpenTrailStash OSM highway classification system.

### POI

A custom POI layer. Custom schema and selection of POI favoring elements useful when biking or
planning bike trips as well as poi often seen on maps to help users orient themeselves. Minzoom
logic for Area-based POI derived from the [Protomaps
Basemaps](https://github.com/protomaps/basemaps) POIs layer.

### Water

This layer is derived from [Protomaps Basemaps](https://github.com/protomaps/basemaps)'s water
layer, physical lines, and physical points layers. It simplifies the tiled data by omiting nearly
all fields except for `intermitent` and `name`.

### Buildings

This layer is derived from [Protomaps Basemaps](https://github.com/protomaps/basemaps)'s buildings
layers, but simplified to exclude all information for 3D rendering (eg: building parts, height).

### Protomaps Basemaps layers

The following layers are included from the [Protomaps
Basemaps](https://github.com/protomaps/basemaps) project without changes:

- Boundaries
- Landuse
- Natural
- Places
- Transit

## Sources

- [OpenStreetMap](http://openstreetmap.org/) the main source of data
- [Natural Earth](https://www.naturalearthdata.com/) for low-zoom water and place data
- [Overture Land Cover Tiles](https://github.com/onthegomap/planetiler-examples/pull/3) for landcover
- icons:
  - [Maki](https://www.mapbox.com/maki-icons/)
  - [Temaki](https://github.com/ideditor/temaki)
  - [OSM Carto](https://github.com/gravitystorm/openstreetmap-carto)
  - [NPS Symbol Library](https://github.com/nationalparkservice/symbol-library)
- Hillshade layer, using public [AWS Terrain Tiles](https://registry.opendata.aws/terrain-tiles/)
- Contours layer, using public [AWS Terrain Tiles](https://registry.opendata.aws/terrain-tiles/),
  powered by [MapLibre Contour](https://github.com/onthegomap/maplibre-contour).

## Map Design

The map design strives to to emphasize unpaved trails and roads.

The 4 main categories from the highway classification system are rendered as follows:
| type | rendering | min zoom |
| ---- | --------- | -------- |
| trail | black dashes with yellow casing | 12 |
| path | white line with yellow casing, grey dashes over the white line if unpaved |
| sidewalk | white line with grey casing | 15 | 11 |
| cut | black dots, no casing | 14 |

### Icon Design

**Color Palette**

| Category                      | Hex Value | Examples                                                                                                                                                                                                                                                                                                          |
| ----------------------------- | --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Transportation                | `#6486f5` | ![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/bike_share.svg)![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/train_station.svg)![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/bike_parking.svg) |
| Shops                         | `#7e53f5` | ![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/bike_shop.svg)![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/outfitter.svg)![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/shop.svg)              |
| Food                          | `#ed7c26` | ![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/restaurant.svg)![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/cafe.svg)![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/convenience_store.svg)     |
| Attractions                   | `#6dad6f` | ![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/jumps.svg)![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/beach.svg)![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/park.svg)                      |
| Red(healthcare/fire/military) | `#d61313` | ![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/hospital.svg)![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/fire_station.svg)![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/airfield.svg)        |
| General                       | `#6e4b4b` | ![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/museum.svg)![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/campground.svg)![](https://gitlab.com/trailstash/openstyle/-/raw/main/style/sprite-src/iconset/svgs/peak.svg)                |

**Modify Icons**

1. Take the `sprite-src/iconset/iconset-trailstash.json` and import it to the [Maki Editor](https://www.mapbox.com/maki-icons/editor/) (working archive.org link: https://web.archive.org/web/20221207102425/https://labs.mapbox.com/maki-icons/editor/).
2. Apply your changes and download the icons in SVG format and the iconset in JSON format.
3. replace the contents of `style/sprite-src/iconset` with the contents of the zip download from the Maki
   Editor.
4. run `npm --prefix style build:sprites`

**Note:** Building sprites uses [`spreet`](https://github.com/flother/spreet)

### Fonts

Currently, the only fonts used are:

- Open Sans Regular
- Open Sans Semibold Italic
- Open Sans Italic

The full ranges for each of these three font stacks are self-hosted. They were copied from the
[`openmaptiles-fonts`](https://github.com/kylebarron/openmaptiles-fonts) repository. These are
downloaded from [openmaptiles/fonts](https://github.com/openmaptiles/fonts/releases).
