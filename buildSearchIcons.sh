#!/bin/bash

set -e

rm -rf static/dist/icons
mkdir -p static/dist/icons

cp style/sprite-src/iconset-full/svgs/* static/dist/icons
