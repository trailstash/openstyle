import MapView from "@trailstash/map-view";
import "@trailstash/map-view/dist/index.css";
import TrailStash from "@trailstash/stylejs-opentrailstash";
import Americana from "@trailstash/stylejs-americana";
import OpenMapTiles from "@trailstash/stylejs-openmaptiles";
import maplibregl from "maplibre-gl";

const searchParams = new URLSearchParams(window.location.search.slice(1));

const root = window.location.origin + window.location.pathname;

let tileUrl = "https://tiles.trailstash.net/opentrailstash.json";
if (searchParams.has("tiles")) {
  tileUrl = searchParams.get("tiles");
}

const view = new MapView({
  map: {
    style: new TrailStash({
      tileUrl,
      landcoverTileUrl: "https://tiles.trailstash.net/overture-land_cover.json",
      sprite: [
        {id: "default", url: `${root}sprites/opentrailstash`},
        {id: "full", url: `${root}sprites/opentrailstash-full`},
      ],
      glyphs:
        "https://trailstash.github.io/openmaptiles-fonts/fonts/{fontstack}/{range}.pbf",
      landmarks: `${root}/landmarks.geojson`,
      goodies: `${root}/goodies.geojson`,
    }),
    container: "map",
    zoom: 14,
    center: [-77.46679, 37.53115],
    hash: true,
    attributionControl: false,
  },
  controls: [
    { type: "NavigationControl", position: "bottom-right" },
    {
      type: "GeolocateControl",
      options: {
        positionOptions: {
          enableHighAccuracy: true,
        },
        trackUserLocation: true,
      },
      position: "bottom-right"
    },
    {
      type: "FontAwesomeLinkControl",
      options: [
        "https://gitlab.com/trailstash/openstyle#opentrailstash",
        "About",
        "info",
      ],
      position: "bottom-right"
    },
    { type: "MapSwapControl" },
    {
      position: "top-left",
      type: "OverpassSearch",
      options: {
        font: "Open Sans Bold",
        categories: [
          {
            name: "BMX/MTB Parks",
            query: `[bbox:{{bbox}}];
            (
              nwr[sport][leisure][sport~"(^|;)(bmx|mtb)(;|$)"];
              nwr[sport][leisure][name][sport~"(^|;)cycling(;|$)"][name~bmx,i];
            );
            out center;`,
            icon: "./dist/icons/shield-jumps.svg",
            sprite: "full:selected-jumps",
          },
          {
            name: "Skate Parks",
            query: `[bbox:{{bbox}}];nwr[sport~"(^|;)skateboard(;|$)"][!shop];out center;`,
            icon: "./dist/icons/shield-skate_park.svg",
            sprite: "full:selected-skate_park",
          },
          {
            name: "Bike Repair Stations",
            query: `[bbox:{{bbox}}];node[amenity=bicycle_repair_station];out geom;`,
            icon: "./dist/icons/shield-bike_repair_station.svg",
            sprite: "full:selected-bike_repair_station",
          },
          {
            name: "Bike Shares",
            query: `[bbox:{{bbox}}];nwr[amenity=bicycle_rental][!shop][bicycle_rental!=shop];out center;`,
            icon: "./dist/icons/shield-bike_share.svg",
            sprite: "full:selected-bike_share",
          },
          {
            name: "Toilets",
            query: `[bbox:{{bbox}}];(nwr[toilets=yes];nwr[amenity=toilets];);out center;`,
            icon: "./dist/icons/shield-toilets.svg",
            sprite: "full:selected-toilets",
          },
          {
            name: "Drinking Water",
            query: `[bbox:{{bbox}}];nwr[amenity=drinking_water];out center;`,
            icon: "./dist/icons/shield-drinking_water.svg",
            sprite: "full:selected-drinking_water",
          },
          {
            name: "Bike Shops",
            query: `[bbox:{{bbox}}];nwr[shop=bicycle];out center;`,
            icon: "./dist/icons/shield-bike_shop.svg",
            sprite: "full:selected-bike_shop",
          },
          {
            name: "Outfitters",
            query: `[bbox:{{bbox}}];nwr[shop=outdoor];out center;`,
            icon: "./dist/icons/shield-outfitter.svg",
            sprite: "full:selected-outfitter",
          },
          {
            name: "Convenience Stores",
            query: `[bbox:{{bbox}}];nwr[shop=convenience];out center;`,
            icon: "./dist/icons/shield-convenience_store.svg",
            sprite: "full:selected-convenience_store",
          },
          {
            name: "Cafés",
            query: `[bbox:{{bbox}}];nwr[amenity=cafe];out center;`,
            icon: "./dist/icons/shield-cafe.svg",
            sprite: "full:selected-cafe",
          },
          {
            name: "Restaurants",
            query: `[bbox:{{bbox}}];nwr[amenity=restaurant];out center;`,
            icon: "./dist/icons/shield-restaurant.svg",
            sprite: "full:selected-restaurant",
          },
          {
            name: "Fast Food",
            query: `[bbox:{{bbox}}];nwr[amenity=fast_food];out center;`,
            icon: "./dist/icons/shield-fast_food.svg",
            sprite: "full:selected-fast_food",
          },
        ],
      },
    },
    {
      type: "AttributionControl",
      options: { compact: false },
      position: "bottom-left"
    },
    {
      type: "ScaleControl",
      options: {
        unit: "imperial",
      },
    },
  ],
  PersistView: true,
  InitializeViewFromIP: {
    IpInfoToken: "a6134fb16df81b",
  },
});

// register service worker for PWA
async function initServiceWorker() {
  if ("serviceWorker" in navigator) {
    try {
      const registration = await navigator.serviceWorker.register("./sw.js");
      await registration.update();
      console.log("Service Worker Registered");
    } catch {}
  }
}
initServiceWorker()

