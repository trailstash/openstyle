import FontAwesomeLinkControl from "./FontAwesomeLinkControl.js";

export default class EditOnOsmControl {
  constructor() {
    this.control = new FontAwesomeLinkControl(
      "https://openstreetmap.org/edit",
      "Edit on OpenStreetMap",
      "pencil",
    );
    this.control.onClick = (e) => {
      const { lat, lng } = this.map.getCenter();
      const zoom = this.map.getZoom();
      e.preventDefault();
      window.open(
        `https://openstreetmap.org/edit/#map=${zoom}/${lat}/${lng}`,
        "_blank",
      );
      return false;
    };
  }
  onAdd(map) {
    this.map = map;
    this.container = this.control.onAdd(map);
    return this.container;
  }
  onRemove() {
    this.control.onRemove();
    this.map = undefined;
    this.container = undefined;
  }
}
