import "@fortawesome/fontawesome-free/js/all.min.js";

export default class FALinkControl {
  constructor(link, description, icon) {
    this.link = link;
    this.description = description;
    this.icon = icon;
  }
  onAdd(map) {
    this._map = map;
    this._container = document.createElement("div");
    this._container.className = `maplibregl-ctrl maplibregl-ctrl-group ${this.icon}`;
    this._container.addEventListener("contextmenu", (e) => e.preventDefault());
    this._container.addEventListener("click", (e) => this.onClick(e));

    this._container.innerHTML = `
      <div class="tools-box">
        <style>
          .fa-control svg.svg-inline--fa {
            color: black;
            height: 16px;
            margin-top: 7px;
          }
        </style>
        <button class="fa-control">
          <a href="${this.link}" target="_blank" rel="noopener">
            <span class="maplibregl-ctrl-icon" aria-hidden="true" title="${this.description}">
              <i class="fa fa-lg fa-${this.icon}"></i>
            </span>
          </a>
        </button>
      </div>
    `;

    return this._container;
  }
  onRemove() {
    this.container.parentNode.removeChild(this.container);
    this.map = undefined;
  }
}
