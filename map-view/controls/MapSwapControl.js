import FontAwesomeLinkControl from "./FontAwesomeLinkControl.js";

export default class MapSwapControl {
  constructor() {
    this.control = new FontAwesomeLinkControl(
      "https://mapswap.trailsta.sh/swap/",
      "View on in another map",
      "shuffle",
    );
  }
  onAdd(map) {
    this.map = map;
    this.container = this.control.onAdd(map);
    const a = this.container.querySelector("a");
    a.target = "_blank";
    const updateLink = (event) => {
      const geoURI = `geo:${this.map.getCenter().lat},${
        this.map.getCenter().lng
      };z=${this.map.getZoom()}`;
      a.href = `https://mapswap.trailsta.sh/swap?min&maps=https://open.trailsta.sh/mapswap.json&url=${geoURI}`;
    };
    updateLink();
    this.map.on("moveend", updateLink);
    return this.container;
  }
  onRemove() {
    this.control.onRemove();
    this.map = undefined;
    this.container = undefined;
  }
}
