import { callInterpreter } from "./overpass.js";
import maplibregl from "maplibre-gl";
import "@fortawesome/fontawesome-free/js/all.min.js";

import "./overpassSearch.css";

const emptyFC = {
  type: "FeatureCollection",
  features: [],
};

const layer = (font) => ({
  id: "OverpassSearchResults",
  type: "symbol",
  source: "OverpassSearchResults",
  layout: {
    "text-field": "{name}",
    "icon-image": ["get", "@icon"],
    "icon-overlap": "always",
    "text-optional": true,
    "text-size": 13,
    "text-radial-offset": 1,
    "text-variable-anchor": ["left", "right", "top", "bottom"],
    "text-justify": "auto",
    "text-font": [font],
    "text-max-width": 6,
  },
  paint: {
    "text-color": "#222",
    "text-halo-blur": 0.5,
    "text-halo-color": "#ffffff",
    "text-halo-width": 2,
  },
});

function renderPopup(features) {
  return (
    "<div>" +
    features
      .map((f) => {
        let html = `<h2 style="margin:0">${f.properties["@type"]} <a target="_blank" href="https://openstreetmap.org/${f.properties["@type"]}/${f.properties["@id"]}">${f.properties["@id"]}</a></h2>`;
        if (f.properties.name) {
          html = `<h2 style="margin:0"><a target="_blank" href="https://openstreetmap.org/${f.properties["@type"]}/${f.properties["@id"]}">${f.properties.name}</a></h2>`;
        }
        html += Object.entries(f.properties)
          .filter(([k, v]) => !k.startsWith("@"))
          .map(([k, v]) => `<code>${k} = ${v}</code>`)
          .join("<br>");
        return html;
      })
      .join("") +
    "</div>"
  );
}

export default class OverpassSearch {
  constructor({ categories, font }) {
    this.categories = categories;
    this.font = font;
    this.data = emptyFC;
  }
  onAdd(map) {
    this._map = map;
    this._container = document.createElement("div");
    this._container.className = `search maplibregl-ctrl`;
    this._container.addEventListener("contextmenu", (e) => e.preventDefault());

    const getSource = () => {
      let source = map.getSource("OverpassSearchResults");
      if (!source) {
        map.addSource("OverpassSearchResults", {
          type: "geojson",
          data: emptyFC,
        });
        source = map.getSource("OverpassSearchResults");
        map.addLayer(layer(this.font));
        map.on("click", "OverpassSearchResults", (e) => {
          const coordinates = e.features[0].geometry.coordinates.slice();
          const description = e.features[0].properties.description;

          new maplibregl.Popup({ offset: 14 })
            .setLngLat(coordinates)
            .setHTML(renderPopup(e.features))
            .addTo(map);
        });

        // Change the cursor to a pointer when the mouse is over the OverpassSearchResults layer.
        map.on("mouseenter", "OverpassSearchResults", () => {
          map.getCanvas().style.cursor = "pointer";
        });

        // Change it back to a pointer when it leaves.
        map.on("mouseleave", "OverpassSearchResults", () => {
          map.getCanvas().style.cursor = "";
        });
      }

      return source;
    };

    map.on("styledata", (event) => {
      if (
        !map.getSource("OverpassSearchResults") &&
        !this.timeout &&
        map.getGlyphs()
      ) {
        this.timeout = setTimeout(() => {
          getSource().setData(this.data);
          this.timeout = null;
        }, 5);
      }
    });

    this._container.innerHTML = ` <ul> </ul> `;
    const ul = this._container.querySelector("ul");
    for (const { name, query, icon, sprite } of this.categories) {
      const li = document.createElement("li");
      li.classList.add("searchPreset");
      li.dataset.query = query;
      li.innerHTML = `
        <a style="cursor:pointer;">
          <img src="${icon}" style="" data-sprite="${sprite}">
            ${name}
          <div class="spinner-xmark">
            <i class="fa fa-lg fa-xmark"></i>
            <i class="fa fa-lg fa-spinner"></i>
          </div
        </a>
      `;
      ul.appendChild(li);
    }

    Array.from(this._container.querySelectorAll("li")).map(
      (el) =>
        (el.onclick = async (event) => {
          getSource().setData(emptyFC);
          if (this.controller) {
            this.controller.abort();
            this.controller = undefined;
          }
          this._container
            .querySelector(".active")
            ?.classList.remove("active", "loading");
          const { currentTarget } = event;
          currentTarget.classList.add("active", "loading");
          const sprite = currentTarget.querySelector("img").dataset.sprite;
          this.controller = new AbortController();
          try {
            this.data = await callInterpreter(
              currentTarget.dataset.query,
              this._map.getBounds(),
              this.controller,
            );
          } catch (err) {
            if (err.name != "AbortError") {
              throw err;
            }
          }
          this.controller = undefined;
          for (const i in this.data.features) {
            this.data.features[i].properties["@icon"] = sprite;
          }
          getSource().setData(this.data);
          currentTarget.classList.remove("loading");
        }),
    );
    map.on("load", () => {
      Array.from(this._container.querySelectorAll("li svg.fa-xmark")).map(
        (el) =>
          (el.onclick = async (event) => {
            event.stopPropagation();
            if (this.controller) {
              this.controller.abort();
              this.controller = undefined;
            }
            this._container
              .querySelector(".active")
              ?.classList.remove("active", "loading");
            this.data = emptyFC;
            getSource().setData(emptyFC);
          }),
      );
    });

    return this._container;
  }
  onRemove() {
    this.container.parentNode.removeChild(this.container);
    this.map = undefined;
  }
}
