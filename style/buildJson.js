import { writeFileSync } from "fs";
import TrailStash from "./index.js";

const [root, tileUrl, hillshade, contours, maptilerKey] = process.argv.slice(2);
const style = new TrailStash({
  sprite: `${root}sprites/opentrailstash`,
  glyphs: `${root}fonts/{fontstack}/{range}.pbf`,
  landcoverTileUrl: "https://tiles.trailstash.net/overture-land_cover.json",
  tileUrl,
  hillshade,
  contours,
  maptilerKey,
});
writeFileSync("style.json", JSON.stringify(style.build()));
