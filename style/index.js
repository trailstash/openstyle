import mlcontour from "maplibre-contour";
import * as pmtiles from "pmtiles";
import {
  transpoLabelLayers,
  tunnelTranspoLayers,
  bridgeTranspoLayers,
  surfaceTranspoLayers,
} from "./layers/transpo.js";
import overtureLandcoverLayer from "./layers/overtureLandcover.js";
import {
  powerLayers,
  undergroundPipelinesLayer,
  pipelinesLayer,
  barrierLayer,
} from "./layers/manmade.js";
import { waterLayers, waterLabelLayers } from "./layers/water.js";
import * as landcoverLayers from "./layers/landcover.js";
import * as landuseLayers from "./layers/landuse.js";
import { contourLayers, maptilerContourLayers } from "./layers/contours.js";
import * as poiLayers from "./layers/poi.js";
import { hillshadeLayer } from "./layers/hillshade.js";
import { boundariesLayers } from "./layers/boundaries.js";
import { placesLayers } from "./layers/places.js";
import buildingsLayer from "./layers/buildings.js";
import { landmarksSource, landmarksLayer } from "./layers/landmarks.js";
import { goodiesSource, goodiesLayer } from "./layers/goodies.js";

export class TrailStash {
  constructor({
    sprite,
    glyphs,
    tileUrl,
    landcoverTileUrl,
    hillshade = true,
    contours = "dem",
    maptilerKey,
    landmarks = "",
    goodies = "",
  }) {
    this.sprite = sprite;
    this.glyphs = glyphs;
    this.tileUrl = tileUrl;
    this.landcoverTileUrl = landcoverTileUrl;
    this.hillshade = hillshade;
    this.contours = contours;
    this.maptilerKey = maptilerKey;
    this.landmarks = landmarks;
    this.goodies = goodies;
  }
  setupMapLibreGL(maplibregl) {
    if (!this.demSource && this.contours == "dem") {
      this.demSource = new mlcontour.DemSource({
        url: "https://s3.amazonaws.com/elevation-tiles-prod/terrarium/{z}/{x}/{y}.png",
        encoding: "terrarium",
        maxzoom: 15,
        worker: true,
        cacheSize: 100,
        timeoutMs: 10_000,
      });
      this.demSource.setupMaplibre(maplibregl);
    }
    if (this.tileUrl.startsWith("pmtiles://")) {
      let protocol = new pmtiles.Protocol();
      maplibregl.addProtocol("pmtiles", protocol.tile);
    }
  }
  setupMap(map) {
    const fetchedImages = {};
    map.on("styleimagemissing", async (ev) => {
      if (ev.id.startsWith("maki/") || ev.id.startsWith("temaki/")) {
        if (!fetchedImages[ev.id]) {
          fetchedImages[ev.id] = true;
          const image = await map.loadImage(
            `./${ev.id}.png`,
          );
          map.addImage(ev.id, image.data, { sdf: true });
        }
      }
    });
  }
  teardown(map, maplibregl) {
    maplibregl.removeProtocol("pmtiles");
  }
  build() {
    let contourSource;
    if (this.demSource && this.contours == "dem") {
      contourSource = {
        type: "vector",
        tiles: [
          this.demSource.contourProtocolUrl({
            // convert meters to feet, default=1 for meters
            multiplier: 3.28084,
            thresholds: {
              // zoom: [minor, major]
              11: [200, 1000],
              12: [100, 500],
              14: [50, 200],
              15: [20, 100],
            },
            contourLayer: "contour",
            elevationKey: "ele",
            levelKey: "level",
            extent: 4096,
            buffer: 1,
          }),
        ],
      };
    } else if (this.contours == "maptiler") {
      contourSource = {
        type: "vector",
        url: `https://api.maptiler.com/tiles/contours/tiles.json?key=${this.maptilerKey}`,
      };
    }
    let terrariumSource;
    if (this.hillshade) {
      if (this.demSource) {
        terrariumSource = {
          type: "raster-dem",
          tiles: [this.demSource.sharedDemProtocolUrl],
          minzoom: 0,
          maxzoom: 15,
          tileSize: 256,
          encoding: "terrarium",
        };
      } else {
        terrariumSource = {
          type: "raster-dem",
          tiles: [
            "https://s3.amazonaws.com/elevation-tiles-prod/terrarium/{z}/{x}/{y}.png",
          ],
          minzoom: 0,
          maxzoom: 15,
          tileSize: 256,
          encoding: "terrarium",
        };
      }
    }
    return {
      id: "opentrailstash",
      version: 8,
      name: "OpenTrailStash",
      sources: {
        trailstash: {
          type: "vector",
          attribution: `<a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap</a>`,
          url: this.tileUrl,
        },
        land_cover: {
          type: "vector",
          attribution: ``,
          url: this.landcoverTileUrl,
        },
        natural_earth_shaded_relief: {
          maxzoom: 6,
          tileSize: 256,
          tiles: [
            "https://trailstash.github.io/naturalearthtiles/tiles/natural_earth_2_shaded_relief.raster/{z}/{x}/{y}.png",
          ],
          type: "raster",
        },
        ...(contourSource ? { contours: contourSource } : {}),
        ...(terrariumSource ? { terrarium: terrariumSource } : {}),
        ...(this.landmarks
          ? { landmarks: landmarksSource(this.landmarks) }
          : {}),
        ...(this.goodies ? { goodies: goodiesSource(this.goodies) } : {}),
      },
      sprite: this.sprite,
      glyphs: this.glyphs,
      layers: [
        {
          id: "background",
          type: "background",
          layout: {
            visibility: "visible",
          },
          paint: {
            "background-color": "rgb(239,239,239)",
          },
        },
        overtureLandcoverLayer,
        landcoverLayers.wetland,
        landcoverLayers.bare_rock,
        landuseLayers.pitch,
        landuseLayers.playground,
        ...(terrariumSource ? [hillshadeLayer] : []),
        ...(contourSource
          ? this.contours == "dem"
            ? contourLayers
            : maptilerContourLayers
          : []),
        ...waterLayers,
        landuseLayers.aerodrome,
        undergroundPipelinesLayer,
        // wilderness/
        ...landuseLayers.parkOutline,
        landuseLayers.military,
        ...tunnelTranspoLayers,
        barrierLayer,
        ...surfaceTranspoLayers,
        pipelinesLayer,
        buildingsLayer,
        ...bridgeTranspoLayers,
        ...transpoLabelLayers,
        ...powerLayers,
        ...boundariesLayers,
        ...waterLabelLayers,
        poiLayers.dot,
        ...placesLayers,
        ...(this.goodies ? [goodiesLayer("goodies")] : []),
        poiLayers.poi,
        ...(this.landmarks ? [landmarksLayer("landmarks")] : []),
      ],
    };
  }
}

export default TrailStash;
