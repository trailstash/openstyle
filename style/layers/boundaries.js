export const boundariesLayers = [
  {
    id: "boundaries_country",
    type: "line",
    source: "trailstash",
    "source-layer": "boundaries",
    filter: ["<=", "pmap:min_admin_level", 2],
    paint: {
      "line-color": "#878787",
      "line-width": 1,
      "line-dasharray": [3, 2],
    },
  },
  {
    id: "boundaries",
    type: "line",
    source: "trailstash",
    "source-layer": "boundaries",
    filter: [">", "pmap:min_admin_level", 2],
    paint: {
      "line-color": "#878787",
      "line-width": 0.5,
      "line-dasharray": [3, 2],
    },
  },
];
