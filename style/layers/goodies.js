export const goodiesSource = (data) => ({ type: "geojson", data });
export const goodiesLayer = (sourceName) => ({
  id: "goodies",
  source: sourceName,
  type: "symbol",
  minzoom: 15,
  layout: {
    "icon-image": ["get", "type"],
    //"text-field": "{name}",
    //"text-font": ["Open Sans Regular"],
    //"text-variable-anchor": ["top"],
    //"text-justify": "auto",
    //"text-radial-offset": 1.25,
    //"text-size": 12,
  },
  paint: {
    //"text-color": "#6e4b4b",
    //"text-halo-color": "white",
    //"text-halo-width": 2,
  },
});
