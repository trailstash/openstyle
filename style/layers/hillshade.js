export const hillshadeLayer = {
  id: "terrain-rgb-terrarium",
  type: "hillshade",
  source: "terrarium",
  minzoom: 0,
  paint: {
    "hillshade-shadow-color": "hsl(39, 21%, 33%)",
    "hillshade-illumination-direction": 315,
    "hillshade-exaggeration": 0.3,
  },
};
