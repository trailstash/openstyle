export const landmarksSource = (data) => ({ type: "geojson", data });
export const landmarksLayer = (sourceName) => ({
  id: "OpenLandmarksMakiTemaki",
  source: sourceName,
  type: "symbol",
  minzoom: 11,
  layout: {
    "icon-image": ["get", "maki_temaki"],
    "text-field": "{name}",
    "text-font": ["Open Sans Regular"],
    "text-variable-anchor": ["top"],
    "text-justify": "auto",
    "text-radial-offset": 1.25,
    "text-size": 12,
    "icon-size": 0.75,
  },
  paint: {
    "icon-color": "#6e4b4b",
    "text-color": "#6e4b4b",
    "icon-halo-color": "white",
    "icon-halo-width": 3,
    "text-halo-color": "white",
    "text-halo-width": 2,
  },
});
