import { green } from "./landcover.js";

export const military = {
  id: "military",
  type: "fill",
  source: "trailstash",
  "source-layer": "landuse",
  filter: ["any", ["==", ["get", "landuse"], "military"]],
  paint: {
    "fill-pattern": "wilderness_polygon",
    "fill-opacity": 0.3,
    "fill-outline-color": "red",
  },
};
export const park = {
  // TODO: this is needed by park outline layers, refactor before delete
  id: "park",
  type: "fill",
  source: "trailstash",
  "source-layer": "landuse",
  filter: [
    "any",
    ["==", ["get", "pmap:kind"], "national_park"],
    ["==", ["get", "pmap:kind"], "nature_reserve"],
    ["==", ["get", "pmap:kind"], "forest"],
    ["==", ["get", "pmap:kind"], "park"],
    ["==", ["get", "pmap:kind"], "dog_park"],
  ],
  paint: {
    "fill-color": "#d8e8c8",
    "fill-opacity": 0.7,
    "fill-outline-color": "rgba(95, 208, 100, 1)",
  },
};
export const parkOutline = [
  {
    ...park,
    id: "park_outline_glow",
    type: "line",
    paint: {
      "line-color": "rgb(182,225,170)",
      "line-width": 2,
      "line-offset": 2,
    },
  },
  {
    ...park,
    id: "park_outline",
    type: "line",
    paint: {
      "line-color": "rgb(143,215,143)",
      "line-width": 2,
    },
  },
];
export const pitch = {
  id: "landuse_pitch",
  type: "fill",
  source: "trailstash",
  "source-layer": "landuse",
  filter: ["all", ["==", "pmap:kind", "pitch"]],
  paint: {
    "fill-color": "hsla(98, 61%, 72%, 1)",
  },
};
export const playground = {
  ...pitch,
  id: "landuse_playground",
  filter: ["all", ["==", "pmap:kind", "playground"]],
};

// TODO REMOVE all below
export const school = {
  id: "landuse_school",
  type: "fill",
  source: "trailstash",
  "source-layer": "landuse",
  filter: ["all", ["==", "pmap:kind", "school"]],
  paint: {
    "fill-color": "rgb(236,238,204)",
  },
};
export const cemetery = {
  id: "landuse_cemetery",
  type: "fill",
  source: "trailstash",
  "source-layer": "landuse",
  filter: ["==", "pmap:Kind", "cemetery"],
  paint: {
    "fill-color": "hsl(75, 37%, 81%)",
  },
};

export const golf = {
  ...green,
  id: "golf",
  "source-layer": "landuse",
  filter: ["==", ["get", "pmap:kind"], "golf_course"],
};
export const aerodrome = {
  id: "aerodrome",
  type: "fill",
  source: "trailstash",
  "source-layer": "landuse",
  minzoom: 11,
  filter: ["==", ["get", "pmap:kind"], "aerodrome"],
  paint: {
    "fill-color": "rgba(229, 228, 224, 1)",
    "fill-opacity": 0.7,
  },
};

export const beach = {
  id: "landuse_beach",
  type: "fill",
  source: "trailstash",
  "source-layer": "landuse",
  filter: ["==", "pmap:kind", "beach"],
  paint: {
    "fill-color": "rgba(247, 239, 195, 1)",
  },
};
