import compose from "./compose.js";
import * as colors from "../colors.js";
import { and, eq, ge, gt, le, lt, get, not } from "../filters.js";

const stylePowerLine = {
  type: "line",
  paint: {
    "line-color": "#858585",
    //"fill-outline-color": colors.waterLabel,
  },
};
const stylePowerTower = {
  type: "symbol",
  layout: {
    "icon-overlap": "always",
    "icon-image": "power_tower",
  },
};
const stylePowerTowerSmall = {
  type: "symbol",
  layout: {
    "icon-overlap": "always",
    "icon-image": "power_tower_small",
  },
};
const stylePowerPole = {
  type: "symbol",
  layout: {
    "icon-overlap": "always",
    "icon-image": "power_pole",
  },
};
const stylePipeline = {
  type: "line",
  paint: {
    "line-dasharray": [10, 1],
    "line-color": "#ccc",
    "line-width": [
      "interpolate",
      ["exponential", 1.6],
      ["zoom"],
      13,
      2,
      20,
      10,
    ],
    //"fill-outline-color": colors.waterLabel,
  },
};
const styleBarrier = {
  type: "line",
  paint: {
    "line-color": "#888",
    "line-width": ["case", eq(get("kind:detail"), "wall"), 1.5, 1],
  },
};

const styleModMinorPowerLine = { paint: { "line-width": 0.5 } };
const styleModMidZoomPowerTower = { layout: { "icon-size": 0.71 } };

const layer = (name) => {
  const baseLayer = {
    id: name,
    source: "trailstash",
    "source-layer": "manmade",
  };
  switch (name) {
    case "pipelines":
      return {
        ...baseLayer,
        filter: eq(get("kind"), "pipeline"),
      };
    case "powerlines":
      return {
        ...baseLayer,
        filter: eq(get("kind"), "power_line"),
      };
    case "minorpowerlines":
      return {
        ...baseLayer,
        filter: eq(get("kind"), "minor_power_line"),
      };
    case "powertowers":
      return {
        ...baseLayer,
        filter: eq(get("kind"), "tower"),
      };
    case "powerpoles":
      return {
        ...baseLayer,
        filter: eq(get("kind"), "pole"),
      };
    case "barriers":
      return {
        ...baseLayer,
        filter: eq(get("kind"), "barrier"),
      };
  }
};

const isLowZoom = (layer) => ({
  ...layer,
  id: layer.id + "_lozoom",
  filter: and(layer.filter, le(["zoom"], 13)),
});
const isHighZoom = (layer) => ({
  ...layer,
  id: layer.id + "_hizoom",
  filter: and(layer.filter, ge(["zoom"], 16)),
});
const isMidZoom = (layer) => ({
  ...layer,
  filter: and(layer.filter, gt(["zoom"], 13), lt(["zoom"], 16)),
});

const isUnderGround = (layer) => ({
  ...layer,
  filter: and(layer.filter, eq(get("kind:detail"), "underground")),
  id: layer.id + "_underground",
});
const isAboveGround = (layer) => ({
  ...layer,
  filter: and(layer.filter, eq(get("kind:detail"), "aboveground")),
});
const styleModeSurfacePipeline = {
  paint: { "line-color": "#aaa" },
};

export const powerLayers = [
  compose(layer("powerlines"), stylePowerLine),
  compose(layer("minorpowerlines"), stylePowerLine, styleModMinorPowerLine),
  compose(
    layer("powertowers"),
    isMidZoom,
    stylePowerTower,
    styleModMidZoomPowerTower,
  ),
  compose(layer("powertowers"), isHighZoom, stylePowerTower),
  compose(layer("powertowers"), isLowZoom, stylePowerTowerSmall),
  compose(layer("powerpoles"), stylePowerPole),
];

export const undergroundPipelinesLayer = compose(
  layer("pipelines"),
  isUnderGround,
  stylePipeline,
);
export const pipelinesLayer = compose(
  layer("pipelines"),
  isAboveGround,
  stylePipeline,
  styleModeSurfacePipeline,
);

export const barrierLayer = compose(layer("barriers"), styleBarrier);
