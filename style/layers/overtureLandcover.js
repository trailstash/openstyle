import { _case, eq, get } from "../filters.js";

export const land_cover = {
  id: "land_cover",
  type: "fill",
  source: "land_cover",
  "source-layer": "land_cover",
  paint: {
    "fill-outline-color": "rgba(0,0,0,0)",
    "fill-color": _case(
      eq(get("subtype"), "urban"),
      "#eaeadb",
      eq(get("subtype"), "forest"),
      "#c9e1b7",
      eq(get("subtype"), "barren"),
      "#f6f6e6",
      eq(get("subtype"), "shrub"),
      "#d2e5b5",
      eq(get("subtype"), "grass"),
      "#e9edc4",
      eq(get("subtype"), "crop"),
      "#e4f6cb",
      eq(get("subtype"), "wetland"),
      "#b2dac9",
      eq(get("subtype"), "mangrove"),
      "#d1e9ca",
      eq(get("subtype"), "moss"),
      "#dee5d1",
      eq(get("subtype"), "snow"),
      "rgb(255, 255, 255)",
      "rgba(255, 255, 255, 0)",
    ),
  },
};
export default land_cover;
