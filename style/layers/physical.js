export const saddle = {
  id: "poi_saddle_ft",
  type: "symbol",
  source: "trailstash",
  "source-layer": "physical",
  filter: ["==", "pmap:kind", "saddle"],
  layout: {
    "symbol-sort-key": ["to-number", ["get", "ele"]],
    "icon-image": "saddle_11",
    "text-anchor": "top",
    "text-field": [
      "case",
      ["all", ["has", "name"], ["has", "ele"]],
      [
        "concat",
        ["get", "name"],
        "\n",
        ["to-string", ["round", ["*", ["to-number", ["get", "ele"]], 3.28084]]],
        "ft",
      ],
      ["has", "name"],
      ["get", "name"],
      "Pass",
    ],
    "text-font": ["Open Sans Regular"],
    "text-max-width": 9,
    "text-offset": [0, 0.6],
    "text-size": 11,
    "icon-allow-overlap": false,
    "icon-size": {
      stops: [
        [14, 0.65],
        [16, 1],
      ],
    },
    visibility: "visible",
  },
  paint: {
    "text-color": "rgba(30, 30, 30, 1)",
    "text-halo-blur": 0.5,
    "text-halo-color": "#ffffff",
    "text-halo-width": 1.5,
  },
};
export const peak = {
  id: "poi_peak_ft",
  type: "symbol",
  source: "trailstash",
  "source-layer": "physical",
  filter: ["==", ["get", "pmap:kind"], "peak"],
  layout: {
    "symbol-sort-key": ["to-number", ["get", "ele"]],
    "icon-image": "mountain_11",
    "text-anchor": "top",
    "text-field": [
      "concat",
      ["get", "name"],
      "\n",
      ["to-string", ["round", ["*", ["to-number", ["get", "ele"]], 3.28084]]],
      "ft",
    ],
    "text-font": ["Open Sans Regular"],
    "text-max-width": 9,
    "text-offset": [0, 0.6],
    "text-size": 11,
    "icon-allow-overlap": false,
    visibility: "visible",
  },
  paint: {
    "text-color": "rgba(30, 30, 30, 1)",
    "text-halo-blur": 0.5,
    "text-halo-color": "#ffffff",
    "text-halo-width": 1.5,
  },
};
export const spring = {
  id: "poi_spring",
  type: "symbol",
  source: "trailstash",
  "source-layer": "physical",
  filter: ["==", ["get", "pmap:kind"], "spring"],
  layout: {
    "icon-image": "water_11",
    "text-anchor": "top",
    "text-field": ["case", ["has", "name"], ["get", "name"], "Spring"],
    "text-font": ["Open Sans Regular"],
    "text-max-width": 9,
    "text-offset": [0, 0.6],
    "text-size": 11,
    "icon-allow-overlap": false,
    "icon-size": {
      stops: [
        [14, 0.65],
        [16, 1],
      ],
    },
  },
  paint: {
    "text-color": "rgba(30, 30, 30, 1)",
    "text-halo-blur": 0.5,
    "text-halo-color": "#ffffff",
    "text-halo-width": 1.5,
  },
};
