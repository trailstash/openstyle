import fs from "fs";

const meta = JSON.parse(fs.readFileSync("metadata.json"));
const base = JSON.parse(fs.readFileSync("iconset-base/iconset-opentrailstash-base.json"));
const colors = JSON.parse(fs.readFileSync("colors.json"));

const metaByName = Object.fromEntries(meta.map((x) => [x.name, x]));

const iconGroup = structuredClone(base.iconGroups[0]);

const full = structuredClone(base);
full.iconGroups = [];
full.iconsetName = full.iconsetName.replace("Base", "Full")

const primaryIcons = structuredClone(base);
primaryIcons.iconGroups = [];
primaryIcons.iconsetName = primaryIcons.iconsetName.replace("-Base", "")

for (const [color, { primary, halo }] of Object.entries(colors)) {
  // Create all fill variants
  const fillIconGroup = structuredClone(iconGroup);
  fillIconGroup.name = `fill-${color}`;
  fillIconGroup.properties = {
    fill: primary,
    bgSize: 4,
    bgFill: "#000",
    bgStrokeWidth: 0,
    strokeWidth: 4,
    bgCircle: false,
    bgRadius: 4,
    stroke: halo || "#ffffff",
    strokeOpt: true,
    bgShape: false,
    bgStroke: "#000",
  };
  fillIconGroup.svgs = Object.fromEntries(
    Object.entries(fillIconGroup.svgs)
      .filter(([name]) => metaByName[name.replace(".svg", "")].color === color)
      .map(([name, icon]) => ["fill-" + name, icon]),
  );
  full.iconGroups.push(fillIconGroup);

  // Create all shielded variants
  const shieldIconGroup = structuredClone(iconGroup);
  shieldIconGroup.name = `shield-${color}`;
  shieldIconGroup.properties = {
    fill: halo || "#ffffff",
    bgSize: 6,
    bgFill: primary,
    bgStrokeWidth: 0,
    strokeWidth: 4,
    bgCircle: false,
    bgRadius: 7,
    stroke: "#ffffff",
    strokeOpt: false,
    bgShape: true,
    bgStroke: halo || "#ffffff",
  };
  shieldIconGroup.svgs = Object.fromEntries(
    Object.entries(shieldIconGroup.svgs)
      .filter(([name]) => metaByName[name.replace(".svg", "")].color === color)
      .map(([name, icon]) => ["shield-" + name, icon]),
  );
  full.iconGroups.push(shieldIconGroup);

  // Create all selected shielded variants
  const selectedIconGroup = structuredClone(iconGroup);
  selectedIconGroup.name = `selected-${color}`;
  selectedIconGroup.properties = {
    fill: halo || "#ffffff",
    bgSize: 6,
    bgFill: primary,
    bgStrokeWidth: 4,
    strokeWidth: 4,
    bgCircle: false,
    bgRadius: 7,
    stroke: "#ffffff",
    strokeOpt: false,
    bgShape: true,
    bgStroke: halo || "#ffffff",
  };
  selectedIconGroup.svgs = Object.fromEntries(
    Object.entries(selectedIconGroup.svgs)
      .filter(([name]) => metaByName[name.replace(".svg", "")].color === color)
      .map(([name, icon]) => ["selected-" + name, icon]),
  );
  full.iconGroups.push(selectedIconGroup);

  // Create primary variants
  const primaryFillIconGroup = structuredClone(iconGroup);
  primaryFillIconGroup.name = `fill-${color}`;
  primaryFillIconGroup.properties = fillIconGroup.properties;
  primaryFillIconGroup.svgs = Object.fromEntries(
    Object.entries(primaryFillIconGroup.svgs)
      .filter(([name]) => metaByName[name.replace(".svg", "")].color === color)
      .filter(([name]) => metaByName[name.replace(".svg", "")].primary === 'fill')
      .map(([name, icon]) => [name, icon]),
  );
  primaryIcons.iconGroups.push(primaryFillIconGroup);
  const primaryShieldIconGroup = structuredClone(iconGroup);
  primaryShieldIconGroup.name = `shield-${color}`;
  primaryShieldIconGroup.properties = shieldIconGroup.properties;
  primaryShieldIconGroup.svgs = Object.fromEntries(
    Object.entries(primaryShieldIconGroup.svgs)
      .filter(([name]) => metaByName[name.replace(".svg", "")].color === color)
      .filter(([name]) => metaByName[name.replace(".svg", "")].primary === 'shield')
      .map(([name, icon]) => [name, icon]),
  );
  primaryIcons.iconGroups.push(primaryShieldIconGroup);
}

if(!fs.existsSync("iconset-full")) {
  fs.mkdirSync("iconset-full");
}
fs.writeFileSync("iconset-full/iconset-opentrailstash-full.json", JSON.stringify(full));

if(!fs.existsSync("iconset")) {
  fs.mkdirSync("iconset");
}
fs.writeFileSync("iconset/iconset-opentrailstash.json", JSON.stringify(primaryIcons));

let readme = `# All Icons

|name|base|primary|fill|shield|selected|source|
|-|-|-|-|-|-|-|
`;
for (const icon in iconGroup.svgs) {
  const name = icon.replace(".svg", "");
  let { source } = metaByName[name];
  source = source.replace(/(https\S*mapbox\/maki\S*icons\/(\S*).svg)/, '[maki:$2]($1) <img height=15 width=15 src="$1">')
  source = source.replace(/(https\S*rapideditor\/temaki\S*icons\/(\S*).svg)/, '[temaki:$2]($1) <img height=15 width=15 src="$1">')
  source = source.replace(/(https\S*gravitystorm\/openstreetmap-carto\S*symbols\/(\S*).svg)/, '[openstreetmap-carto:$2]($1) <img height=15 width=15 src="$1">')
  source = source.replace(/(https\S*nationalparkservice\/symbol-library\S*src\/(\S*).svg)/, '[nps-symbol-library:$2]($1) <img height=15 width=15 src="$1">')
  source = source.replace(/(https\S*osm-americana\/openstreetmap-americana\S*icons\/(\S*).svg)/, '[openstreetmap-americana:$2]($1) <img height=15 width=15 src="$1">')
  
  readme += `|\`${name}\`|`;
  readme += `![](./iconset-base/svgs/${icon})|`;
  readme += `![](./iconset/svgs/${icon})|`;
  readme += `![](./iconset-full/svgs/fill-${icon})|`;
  readme += `![](./iconset-full/svgs/shield-${icon})|`;
  readme += `![](./iconset-full/svgs/selected-${icon})|`;
  readme += `${source}|`;
  readme += `\n`;
}
readme += `

# Old vs New
|name|old|new|
|-|-|-|
`;
const old = JSON.parse(fs.readFileSync("iconset-old/iconset-opentrailstash.json"));
for (const iconGroup of old.iconGroups) {
  for (const icon in iconGroup.svgs) {
    const name = icon.replace(".svg", "");
    
    readme += `|\`${name}\`|`;
    readme += `![](./iconset-old/svgs/${icon})|`;
    readme += `![](./iconset/svgs/${icon})|`;
    readme += `\n`;
  }
}

fs.writeFileSync("README.md", readme);
