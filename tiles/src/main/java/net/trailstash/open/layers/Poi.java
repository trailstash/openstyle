package net.trailstash.open.layers;

import static java.util.Map.entry;

import com.onthegomap.planetiler.FeatureCollector;
import com.onthegomap.planetiler.ForwardingProfile;
import com.onthegomap.planetiler.VectorTile;
import com.onthegomap.planetiler.geo.GeoUtils;
import com.onthegomap.planetiler.geo.GeometryException;
import com.onthegomap.planetiler.reader.SourceFeature;
import com.onthegomap.planetiler.util.ZoomFunction;
import com.protomaps.basemap.feature.FeatureId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Poi implements ForwardingProfile.FeatureProcessor, ForwardingProfile.FeaturePostProcessor {
  // min zooms for different types (ignoring area)
  static Map<String, Integer> minZooms = Map.ofEntries(
    // shops
    entry("bike_shop", 12),
    entry("outfitter", 12),
    entry("bike_rental", 13),
    entry("shop", 15),
    // transportation
    entry("bike_repair_station", 12),
    entry("airport", 11),
    entry("airfield", 12), // TODO: icon
    entry("airstrip", 13), // TODO: icon
    entry("ferry_terminal", 13),
    entry("bus_station", 13),
    entry("train_station", 13),
    entry("bike_share", 15),
    entry("bus_stop", 16),
    entry("bike_parking", 16),
    entry("parking", 16),
    // water
    entry("drinking_water", 14),
    entry("waterfall", 13),
    entry("spring", 13),
    // food
    entry("convenience_store", 13),
    entry("grocery_store", 13),
    entry("restaurant", 15),
    entry("bakery", 15),
    entry("cafe", 15),
    entry("ice_cream", 15),
    entry("fast_food", 15),
    // general
    entry("ranger_station", 12),
    entry("quarry", 14),
    entry("cemetery", 14),
    entry("shelter", 15),
    entry("picnic_shelter", 15),
    entry("transit_shelter", 16),
    entry("wilderness_hut", 13),
    entry("campground", 12),
    entry("campsite", 14),
    entry("firepit", 16),
    entry("lodging", 14),
    entry("observation_tower", 13),
    entry("lighthouse", 13),
    entry("post_office", 14),
    entry("library", 14),
    entry("town_hall", 14),
    entry("museum", 14),
    entry("school", 14),
    entry("ghost_bike", 15),
    // from protomaps POIS:
    // One would think University should be earlier, but there are lots of dinky node only places
    // So if the university has a large area, it'll naturally improve it's zoom in the next section...
    entry("university", 14),
    entry("passport_control", 14),
    entry("info_office", 14),
    entry("info_pole", 16),
    entry("guide_post", 15),
    entry("info_board", 15),
    entry("information", 16),
    entry("viewpoint", 15),
    entry("courthouse", 14),
    entry("place_of_worship", 14),
    entry("muslim_place_of_worship", 14),
    entry("jewish_place_of_worship", 14),
    entry("christian_place_of_worship", 14),
    entry("shinto_place_of_worship", 14),
    entry("buddhist_place_of_worship", 14),
    entry("police", 14),
    entry("bank", 14),
    entry("pub", 14),
    entry("bar", 14),
    entry("toilets", 14),
    entry("atm", 16),
    entry("picnic_area", 14),
    entry("picnic_table", 15),
    entry("bench", 15),
    entry("recycling", 15),
    entry("trash_bin", 15),
    entry("gate", 15),
    // attraction
    entry("national_park", 12),
    entry("park", 14),
    entry("dog_park", 15),
    entry("skate_park", 12),
    entry("jumps", 12),
    entry("community_garden", 13),
    entry("peak", 13),
    entry("saddle", 13),
    entry("playground", 13),
    entry("garden", 14),
    entry("beach", 14),
    // "red"
    entry("hospital", 12),
    entry("pharmacy", 14),
    entry("clinic", 14),
    entry("fire_station", 14),
    entry("veterinary", 14)
  );
  // poi catagory mappings
  static Map<String, String> categories = Map.ofEntries(
    // shops
    entry("bike_shop", "shop"),
    entry("outfitter", "shop"),
    entry("bike_rental", "shop"),
    entry("shop", "shop"),
    // transportation
    entry("bike_repair_station", "transportation"),
    entry("airport", "transportation"),
    entry("airstrip", "transportation"),
    entry("ferry_terminal", "transportation"),
    entry("bus_station", "transportation"),
    entry("train_station", "transportation"),
    entry("bike_share", "transportation"),
    entry("bus_stop", "transportation"),
    entry("bike_parking", "transportation"),
    entry("parking", "transportation"),
    // water
    entry("drinking_water", "water"),
    entry("waterfall", "water"),
    entry("spring", "water"),
    // food
    entry("convenience_store", "food"),
    entry("grocery_store", "food"),
    entry("restaurant", "food"),
    entry("bakery", "food"),
    entry("cafe", "food"),
    entry("ice_cream", "food"),
    entry("fast_food", "food"),
    entry("pub", "food"),
    entry("bar", "food"),
    // general
    entry("quarry", "general"),
    entry("cemetery", "general"),
    entry("shelter", "general"),
    entry("picnic_shelter", "general"),
    entry("transit_shelter", "general"),
    entry("wilderness_hut", "general"),
    entry("campground", "general"),
    entry("campsite", "general"),
    entry("firepit", "general"),
    entry("lodging", "general"),
    entry("observation_tower", "general"),
    entry("lighthouse", "general"),
    entry("post_office", "general"),
    entry("library", "general"),
    entry("museum", "general"),
    entry("school", "general"),
    entry("university", "general"),
    entry("passport_control", "general"),
    entry("information", "general"),
    entry("info_office", "general"),
    entry("info_board", "general"),
    entry("guide_post", "general"),
    entry("info_pole", "general"),
    entry("ranger_station", "general"),
    entry("viewpoint", "general"),
    entry("town_hall", "general"),
    entry("courthouse", "general"),
    entry("place_of_worship", "general"),
    entry("muslim_place_of_worship", "general"),
    entry("jewish_place_of_worship", "general"),
    entry("christian_place_of_worship", "general"),
    entry("shinto_place_of_worship", "general"),
    entry("buddhist_place_of_worship", "general"),
    entry("police", "general"),
    entry("bank", "general"),
    entry("toilets", "general"),
    entry("atm", "general"),
    entry("picnic_area", "general"),
    entry("picnic_table", "general"),
    entry("bench", "general"),
    entry("recycling", "general"),
    entry("trash_bin", "general"),
    entry("gate", "general"),
    // attraction
    entry("national_park", "attraction"),
    entry("park", "attraction"),
    entry("dog_park", "attraction"),
    entry("skate_park", "attraction"),
    entry("jumps", "attraction"),
    entry("peak", "attraction"),
    entry("saddle", "attraction"),
    entry("garden", "attraction"),
    entry("community_garden", "attraction"),
    entry("playground", "attraction"),
    entry("beach", "attraction"),
    // "red"
    entry("hospital", "red"),
    entry("pharmacy", "red"),
    entry("clinic", "red"),
    entry("fire_station", "red"),
    entry("veterinary", "red"),
    entry("airfield", "red")
  );
  // poi ordering
  static ArrayList<String> order = new ArrayList(Arrays.asList(new String[]{
    // big parks!
    "national_park",
    // major features
    "airport",
    "airfield",
    "ferry_terminal",
    "bus_station",
    "train_station",
    // major shops
    "bike_shop",
    "outfitter",
    "bike_rental",
    // primary attractions
    "jumps",
    "skate_park",
    // healtcare
    "hospital",
    "pharmacy",
    "clinic",
    // "standard" map POI
    "museum",
    "post_office",
    "library",
    "school",
    "university",
    "town_hall",
    "courthouse",
    "fire_station",
    "lodging",
    // campgrounds are importanter than parks
    "campground",
    // attractions
    "park",
    "beach",
    "community_garden",
    "garden",
    "playground",
    "dog_park",
    // bigger park amenities
    "info_office",
    "ranger_station",
    // bike share & repair
    "bike_repair_station",
    "bike_share",
    // other
    "observation_tower",
    "lighthouse",
    "passport_control",
    "airstrip",
    // Food
    "convenience_store",
    "grocery_store",
    "cafe",
    "bakery",
    "ice_cream",
    "restaurant",
    "drinking_water",
    "fast_food",
    "pub",
    "bar",
    // dog health care
    "veterinary",
    // money
    "bank",
    "atm",
    // generic shop
    "shop",
    // Natural features
    "waterfall",
    "spring",
    "peak",
    "saddle",
    // Landuses
    "quarry",
    "cemetery",
    // camping/picnicing/sheltering/toilets
    "wilderness_hut",
    "shelter",
    "campsite",
    "picnic_area",
    "picnic_shelter",
    "firepit",
    "toilets",
    // places of worship
    "place_of_worship",
    "muslim_place_of_worship",
    "jewish_place_of_worship",
    "christian_place_of_worship",
    "shinto_place_of_worship",
    "buddhist_place_of_worship",
    // ACAB
    "police",
    // bus stops & shelters
    "bus_stop",
    "transit_shelter",
    // Small POI
    "picnic_table",
    "guide_post",
    "recycling",
    "trash_bin",
    "bench",
    "info_board",
    "info_pole",
    "information",
    "viewpoint",
    "gate",
    // parking
    "bike_parking",
    "parking"
  }));


  public Poi() {}

  @Override
  public String name() {
    return "poi";
  }

  private static final double WORLD_AREA_FOR_70K_SQUARE_METERS =
    Math.pow(GeoUtils.metersToPixelAtEquator(0, Math.sqrt(70_000)) / 256d, 2);
  private static final double LOG2 = Math.log(2);

  /*
   * Assign every toilet a monotonically increasing ID so that we can limit output at low zoom levels to only the
   * highest ID toilet nodes. Be sure to use thread-safe data structures any time a profile holds state since multiple
   * threads invoke processFeature concurrently.
   */
  private final AtomicInteger poiNumber = new AtomicInteger(0);

  @Override
  public void processFeature(SourceFeature sf, FeatureCollector features) {
    String kind = "";
    String name = sf.getString("name");

    // Fuck Trump
    if (sf.getString("wikidata") == "Q130018") {
      name = "Denali";
    }

    if (sf.isPoint() || sf.canBePolygon()) {
      // water
      if (sf.isPoint() && sf.hasTag("amenity", "drinking_water")) {
        kind = "drinking_water";
        // natural features
      } else if (sf.hasTag("natural", "beach")) {
        kind = sf.getString("natural"); // using OSM tag value
      } else if (sf.isPoint() && sf.hasTag("natural", "peak", "saddle", "spring")) {
        kind = sf.getString("natural"); // using OSM tag value
      } else if (sf.hasTag("waterway", "waterfall")) {
        kind = "waterfall";
        // Bike repair stations
      } else if (sf.hasTag("amenity", "bicycle_repair_station")) {
        kind = "bike_repair_station";
        name = "";
        // Ghost Bikes
      } else if (sf.hasTag("memorial", "ghost_bike") && sf.hasTag("historic")) {
        kind = "ghost_bike";
        name = ""; // for the privacy of the deceased.
        // barriers
      } else if (sf.hasTag("barrier", "gate", "kiss_gate")) {
        kind = "gate";
          // parks
      } else if (sf.hasTag("boundary", "national_park")) {
        kind = "national_park";
      } else if (sf.hasTag("boundary", "protected_area")) {
        if (!sf.hasTag("name")) {
            return;
        }
        kind = "park";
      } else if (sf.hasTag("leisure", "park", "nature_reserve")) {
        if (!sf.hasTag("name")) {
            return;
        }
        kind = "park";
      } else if (sf.hasTag("leisure", "garden")) {
        if (!sf.hasTag("name")) {
            return;
        }
        if (sf.hasTag("garden:type", "community")) {
            kind = "community_garden";
        } else {
            kind = sf.getString("leisure"); // using OSM tag value
        }
      } else if (sf.hasTag("leisure", "playground", "dog_park")) {
        kind = sf.getString("leisure"); // using OSM tag value
        // skateparks
      } else if (!sf.hasTag("shop") && sf.hasTag("sport", "skateboard")) {
        kind = "skate_park";
        // other land use
      } else if (sf.hasTag("landuse", "cemetery") || sf.hasTag("amenity", "grave_yard")) {
        kind = "cemetery";
      } else if (sf.hasTag("landuse", "quarry")) {
        kind = "quarry";
      } else if (sf.hasTag("aeroway", "aerodrome")) {
        if (sf.hasTag("military") && !sf.hasTag("military", "no")) {
          kind = "airfield";
        } else if (sf.hasTag("iata")) {
          kind = "airport";
        } else {
          kind = "airstrip";
        }
      } else if (sf.hasTag("aeroway", "airstrip")) {
        kind = "airstrip";
        // trains
      } else if (sf.hasTag("railway", "station")) {
        kind = "train_station";
        // shops
      } else if (sf.hasTag("shop", "bicycle")) {
        kind = "bike_shop";
      } else if (sf.hasTag("shop", "outdoor")) {
        kind = "outfitter";
      } else if (sf.hasTag("shop", "convenience")) {
        kind = "convenience_store";
      } else if (sf.hasTag("shop", "supermarket")) {
        kind = "grocery_store";
      } else if (sf.hasTag("shop", "bakery", "ice_cream")) {
        kind = sf.getString("shop"); // using OSM tag value
        // amenity
      } else if (sf.hasTag("amenity", "place_of_worship")) {
          if (sf.hasTag("religion", "christian", "jewish", "muslim", "buddhist", "shinto")) {
              kind = sf.getString("religion") + "_place_of_worship";
          } else {
              kind = "place_of_worship";
          }
      } else if (sf.hasTag("amenity", "bicycle_rental")) {
        if (sf.hasTag("shop") || sf.hasTag("bicycle_rental", "shop")) {
          kind = "bike_rental";
        } else {
          kind = "bike_share";
          name = "";
        }
      } else if (sf.hasTag("amenity", "shelter")) {
        if (sf.hasTag("shelter_type", "picnic")) {
          kind = "picnic_shelter";
        } else if (sf.hasTag("shelter_type", "public_transport")) {
          kind = "transit_shelter";
          name = "";
        } else {
          kind = "shelter";
        }
      } else if (sf.hasTag("amenity", "townhall")) {
        kind = "town_hall";
      } else if (sf.hasTag("amenity", "pub", "biergarten") || sf.hasTag("craft", "brewery")) {
        kind = "pub";
      } else if (sf.hasTag("amenity", "school", "kindergarten")) {
        kind = "school";
      } else if (sf.hasTag("amenity", "university", "college")) {
        kind = "university";
      } else if (sf.hasTag("amenity", "bicycle_parking")) {
        kind = "bike_parking";
        name = "";
      } else if (sf.hasTag("amenity", "waste_basket")) {
        name = "";
        kind = "trash_bin";
      } else if (sf.hasTag("amenity", "townhall")) {
        kind = "town_hall";
      } else if (sf.hasTag("amenity", "bus_station", "ferry_terminal", "restaurant", "ice_cream", "cafe", "fast_food",
        "clinic", "hospital", "pharmacy", "post_office", "library", "fire_station", "bank", "bar", "toilets", "atm",
        "recycling", "bench", "parking", "courthouse", "police", "ranger_station", "veterinary")) {
        kind = sf.getString("amenity"); // using OSM tag value
        // generic shop (later, incase they're things like bike rental facilities too)
      } else if (sf.hasTag("shop")) {
        kind = "shop";
        // bus stops
      } else if (sf.hasTag("highway", "bus_stop")) {
        kind = "bus_stop";
        name = "";
      } else if (sf.hasTag("barrier", "border_control")) {
        kind = "passport_control";
        // man made
      } else if (sf.hasTag("man_made", "lighthouse")) {
        kind = "lighthouse";
      } else if (sf.hasTag("man_made", "tower") && sf.hasTag("tower:type", "observation")) {
        kind = "observation_tower";
        // tourism
      } else if (sf.hasTag("tourism", "camp_site")) {
        kind = "campground";
      } else if (sf.hasTag("tourism", "camp_pitch")) {
        kind = "campsite";
      } else if (sf.hasTag("tourism", "wilderness_hut")) {
        kind = "wilderness_hut";
      } else if (sf.hasTag("tourism", "picnic_site")) {
        kind = "picnic_area";
      } else if (sf.hasTag("tourism", "hotel", "motel", "alpine_hut", "guest_house", "hostel")) {
        kind = "lodging";
      } else if (sf.hasTag("tourism", "information")) {
        if (sf.hasTag("information", "visitor_centre", "visitor_center", "office")) {
          kind = "info_office";
        } else if (sf.hasTag("information", "map", "board")) {
          kind = "info_board";
        } else if (sf.hasTag("information", "stele", "terminal")) {
          kind = "info_pole";
        } else if (sf.hasTag("information", "guidepost")) {
          kind = "guide_post";
        } else if (!sf.hasTag("information")) {
          kind = "information";
        }
      } else if (sf.hasTag("tourism", "museum", "information", "viewpoint")) {
        kind = sf.getString("tourism"); // using OSM tag value
        // non-park leisure
      } else if (sf.hasTag("leisure", "firepit")) {
        kind = sf.getString("leisure"); // using OSM tag value
      } else if (sf.hasTag("leisure", "picnic_table")) {
        kind = "picnic_table";
        // Jumps!
      } else if (sf.hasTag("leisure") && sf.hasTag("sport")) {
        // ^ was a _very_ crude filter, but faster. Need to add ; support to sf.hasTag!
        var sport = sf.getString("sport");
        if (sport.contains("bmx") || sport.contains("mtb") || (sport.contains("cycling") && sf.hasTag("name") && name.contains("BMX"))) {
          kind = "jumps";
        }
      }
    }
    if (kind.equals("")) {
      return;
    }

    Integer minZoom = this.minZooms.get(kind);
    if (kind.equals("peak")) {
      if (sf.hasTag("wikidata")) {
        minZoom -= 2;
      }
    } else if (sf.canBePolygon()) {
      Double wayArea = 0.0;
      try {
        wayArea = sf.worldGeometry().getEnvelopeInternal().getArea() / WORLD_AREA_FOR_70K_SQUARE_METERS;
      } catch (GeometryException e) {
        e.log("Exception in POI way calculation");
      }

      // Area zoom grading overrides the kind zoom grading in the section above.
      // Roughly shared with the water label area zoom grading in physical points layer
      //
      // Allowlist of kind values eligible for early zoom point labels
      if (kind.equals("national_park") || kind.equals("park") || kind.equals("school") || kind.equals("university") ||
        kind.equals("quarry") || kind.equals("cemetery") || kind.equals("airport")) {
        if (wayArea > 300000) { // 500000000 sq meters (web mercator proj)
          minZoom = 5;
        } else if (wayArea > 25000) { // 500000000 sq meters (web mercator proj)
          minZoom = 6;
        } else if (wayArea > 10000) { // 500000000
          minZoom = 7;
        } else if (wayArea > 2000) { // 200000000
          minZoom = 8;
        } else if (wayArea > 250) { //  40000000
          minZoom = 9;
        } else if (wayArea > 100) { //   8000000
          minZoom = 10;
        } else if (wayArea > 20) { //    500000
          minZoom = 11;
        } else if (wayArea > 5) {
          minZoom = 12;
        } else if (wayArea > 1) {
          minZoom = 13;
        } else if (wayArea > 0.25) {
          minZoom = 14;
        }
      }
    }

    FeatureCollector.Feature feat;
    if (sf.canBePolygon()) {
      feat = features.pointOnSurface(this.name());
    } else {
      feat = features.point(this.name());
    }
    // all POIs should receive their IDs at all zooms
    // (there is no merging of POIs like with lines and polygons in other layers)
    feat.setId(FeatureId.create(sf));
    // Core Tilezen schema properties
    feat.setAttr("pmap:kind", kind);
    feat.setAttr("category", this.categories.get(kind));
    feat.setAttr("pmap:min_zoom", minZoom);
    feat.setAttr("order", this.order.indexOf(kind));
    feat.setZoomRange(Math.min(15, minZoom), 16);
    feat.setBufferPixels(128);

    if (name != null && name != "") {
      feat.setAttr("name", name);
    }

    // Server sort features so client label collisions are pre-sorted
    feat.setSortKey(this.order.indexOf(kind) * 1000000000 + poiNumber.incrementAndGet());

    // Even with the categorical zoom bucketing above, we end up with too dense a point feature spread in downtown
    // areas, so cull the labels which wouldn't label at earlier zooms than the max_zoom of 15
    feat.setPointLabelGridSizeAndLimit(13, 10, 1);

    // and also whenever you set a label grid size limit, make sure you increase the buffer size so no
    // label grid squares will be the consistent between adjacent tiles
    feat.setBufferPixelOverrides(ZoomFunction.maxZoom(13, 32));

    return;
  }

  @Override
  public List<VectorTile.Feature> postProcess(int zoom, List<VectorTile.Feature> items) throws GeometryException {

    // TODO: (nvkelso 20230623) Consider adding a "pmap:rank" here for POIs, like for Places

    return items;
  }
}
